![Darkness](https://img.shields.io/badge/jekyll-Darkness-%23222)
![Gem Version](https://img.shields.io/badge/Gem-v%202.6.3-red)
![Build Status](https://travis-ci.com/rokhimin//jekyll-darkness.svg?branch=master)
![Coverage Status](https://img.shields.io/badge/coverage-99%25-green)

# Darkness
## About
blog static using jekyll inspired h20

###### Live
- host heroku : https://rokhimin-darkness.herokuapp.com
- host netlify : https://rokhimin-darkness.netlify.com

## Deploy

###### Heroku
[![Deploy to heroku](https://www.herokucdn.com/deploy/button.png)](https://dashboard.heroku.com/new?button-url=https://github.com/rokhimin/Darkness/tree/deploy_heroku&template=https://github.com/rokhimin/jekyll-darkness/tree/deploy_heroku) 

###### Netlify
 [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/rokhimin/jekyll-netlify)

## Test Locally
- ``bundle install``
- ``bundle exec jekyll s``

## Another Version
[Jekyll Darkness SinglePage](https://github.com/rokhimin/jekyll-darkness-singlepage)

## License 
MIT License.
